﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pickupSpawning : MonoBehaviour {

    public GameObject pickupPrefab;
    public float xMin, xMax, yMin, yMax;




    // Use this for initialization
    void Start() {
        for (int i = 0; i <= 12; i++)
        {
            SpawnPickup();
        }
        //Invoke("SpawnRepeat", 5f); Spawns a pickup every 5 seconds
	}
	
	// Update is called once per frame
	void Update () {
		
	}



    public   void SpawnPickup()
    {

        Vector3 randomPos = new Vector3(Random.Range(xMin, xMax), pickupPrefab.transform.position.y, Random.Range(yMin, yMax));
        Instantiate(pickupPrefab, randomPos, pickupPrefab.transform.rotation);

    }
    void SpawnRepeat()
    {
        SpawnPickup();
        Invoke("SpawnRepeat", 5f);
    }
    
}
