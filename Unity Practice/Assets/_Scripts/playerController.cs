﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class playerController : MonoBehaviour {
    Rigidbody rb;
    int count = 0;
    public float speed;
    public Text countText;
    public Text winText;
    public pickupSpawning pickupSpawn;
    float duration = 1.5f;
    float t = 0;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
        setCountText();
	}
	
	// Update is called once per frame
	void Update () {
	}
    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
        Vector3 movement = new Vector3(moveHorizontal, 0f, moveVertical);
        rb.AddForce(movement * 10);
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pickup"))
        {
            count += 1;
            pickupSpawn.SpawnPickup();
            setCountText();
            Destroy(other.gameObject);
        }
        if (other.gameObject.CompareTag("Death"))
        {
            Destroy(gameObject);
            winText.text = "YOU LOSE!!!";
        }
    }
    void setCountText()
    {
        countText.text = "Count: " + count.ToString();
        if (count >= 30)
        {
            winText.text = "YOU WIN!!!";
        }
    }
    
}
